package startit.rest.ex2;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.sql.DataSource;


//declaram aplicatia ca fiind una spring
@SpringBootApplication
public class main {

    public static void main(String[] args) {

        //Create Spring application context
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:/spring.xml");

        //linia de mai jos in combinatie cu fisierul application.properties a facut posibilia
        //conexiunea la baza de data
        //intelegerea mea este ca linia de mai jos, in momentul conexiuni ii spune la ce baza de date
        //sa se conecteze
        DataSource dataSource = (DataSource) ctx.getBean("dataSource");

        SpringApplication.run(main.class, args);



    }
}
