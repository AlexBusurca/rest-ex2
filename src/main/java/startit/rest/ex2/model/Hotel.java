package startit.rest.ex2.model;


import org.springframework.stereotype.Component;

import javax.persistence.*;

//folosesc @component ca sa pot face autowired la nevoie
@Component
@Entity //declar acest POJO ca obiect ce va fi legat de tabela hotels creata de hsqldb pe baza hotels
@Table(name = "hotels", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")})//cheie unica coloana ID
public class Hotel {
    @Id//variabila id va fi legata de cheia unica declarata mai sus
    //si valoarea ei va fi generata automat de generatorul de mai sus
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    private String name;
    private Integer stars;
    private Float rating;

    //in continuare avem constructori, setteri si getteri pentru variabile si
    //un tostring pentru afisare inteligibila a datelor

    public Hotel(){}

    public Hotel(String name, Integer stars, Float rating) {
        this.name = name;
        this.stars = stars;
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String toString() {
        return "Hotel ID: " + getId() + ";\nHotel name: " + getName() + ";\nHotel stars: " + getStars() + ";\nHotel rating: " + getRating() +"\n";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    }
