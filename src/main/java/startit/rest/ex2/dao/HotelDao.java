package startit.rest.ex2.dao;

import startit.rest.ex2.model.Hotel;

import java.util.List;

public interface HotelDao {
    boolean idExists(Integer id);
    List<Hotel> returnHotelList();
    Hotel returnHotel(Integer id) throws Exception;
    Hotel newHotel(String name, Integer stars, Float rating) throws Exception;
    Hotel modifyHotelByID(Integer id, Integer stars, Float rating) throws Exception;
    Hotel removeHotel(Integer id) throws Exception;

}
