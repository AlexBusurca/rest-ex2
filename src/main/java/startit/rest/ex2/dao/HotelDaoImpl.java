package startit.rest.ex2.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import startit.rest.ex2.model.Hotel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

//componenta spring de tip repository
@Repository
public class HotelDaoImpl implements HotelDao {

    //folosesc autowired ca sa nu trebuiasca sa initialize un nou hotel de fiecare data cand am nevoie
    @Autowired
    private Hotel hotel;

    //datele din URL vor fi comise pe baza cu ajutorul acestui em
    @PersistenceContext
    private EntityManager em;

    //metoda pe care o folosesc pentru a trata situatia in care id-ul introdus nu exista in baza de date
    @Transactional
    public boolean idExists(Integer id) {
        Long count = (Long) em.createQuery("Select count(h) from Hotel h where h.id=:id")
                    .setParameter("id", id)
                    .getSingleResult();
        return ((count.equals(0L)) ? false:true);
    }

    //metodele sunt tranzactii pe baza gestionate de spring
    @Transactional
    public List<Hotel> returnHotelList() {
        List<Hotel> hotels = em.createQuery("from Hotel")
                .getResultList();
        em.close();
        return hotels;
    }

    //aduc din baza obiectul pe baza id-ului introdus de user
    @Transactional
    public Hotel returnHotel(Integer id) throws Exception {
        try {
                hotel = em.createQuery("Select h from Hotel h where h.id=:id", Hotel.class)
                        .setParameter("id", id)
                        .getSingleResult();
            } catch (Exception e) {}
        return hotel;
    }

    //metoda care persista un hotel nou in baza de date conform informatiilor furnizate de user
    @Transactional
    public Hotel newHotel(String name, Integer stars, Float rating) throws Exception {
         hotel = new Hotel(name, stars, rating);
            try {
            em.persist(hotel);
            } catch(Exception e){}
            return hotel;
    }

    //metoda care intoarce din baza un obiect pe baza id-ului introdus de user si
    //modifica informatiile despre el pe baza a ceea ce introduce userul
    @Transactional
    public Hotel modifyHotelByID(Integer id, Integer stars, Float rating) throws Exception {
        try {
            hotel = returnHotel(id);
        } catch (Exception e) {}
        try{
            hotel.setStars(stars);
            hotel.setRating(rating);
            em.persist(hotel);
            em.close();
        } catch(Exception e1) {}
        return hotel;
    }

    //intoarce din baza un hotel pe baza id-ului introdus de user si apoi il sterge din baza
    @Transactional
    public Hotel removeHotel(Integer id) throws Exception {
        try {
            hotel = returnHotel(id);
        } catch (Exception e) {}
        try {
            em.createQuery("delete from Hotel h where id=:id")
                    .setParameter("id", id)
                    .executeUpdate();
        } catch (Exception e) {}
        return hotel;
    }
}
