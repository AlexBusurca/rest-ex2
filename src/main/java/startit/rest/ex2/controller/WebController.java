package startit.rest.ex2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import startit.rest.ex2.dao.HotelDao;
import startit.rest.ex2.model.Hotel;

//declar clasa ca obiect ce va lega url-ul de obiectul hotel
@RestController
public class WebController {

    //folosesc autowired ca sa nu initializez obiecte de fiecare data cand am nevoie
    @Autowired
    HotelDao hotelDao;
    @Autowired
    Hotel hotel;

    //cand este accesat url-ul cu HotelList este afisat un tostring de lista ce contine elementele
    // rezulate in urma selectului facut pe baza in memory
    @RequestMapping(value = "/hotels/HotelList/get", method = RequestMethod.GET)
    public String getAllHotels() {
        return hotelDao.returnHotelList().toString();
    }

    //aici, userul poate cere sa vada datele pentru un anumit hotel daca cunoaste id-ul
    @RequestMapping(value = "/hotels/getHotel/get/{id}", method = RequestMethod.GET)
    public String getHotel(@PathVariable(value = "id") Integer id) {
        if (hotelDao.idExists(id)) {
            try {
                hotel = hotelDao.returnHotel(id);
            } catch (Exception e) {
                return e.toString();
            }
            return "Hotel with ID: " + id + " has the following details: "
                    + "\n Name - " + hotel.getName()
                    + "\n Stars - " + hotel.getStars()
                    + "\n Rating - " + hotel.getRating();
        } else {
            return "Hotel with ID: " + id + " does not exist!";
        }
    }

    //aici usuerul poate adauga hoteluri noi folosind postman
    @RequestMapping(value = "/hotels/addHotel/post", method = RequestMethod.POST)
    public String addHotel(@RequestParam("name") String name,
                           @RequestParam("rating") Float rating,
                           @RequestParam("stars") Integer stars) {
        try {
            hotel = hotelDao.newHotel(name, stars, rating);
        } catch (Exception e) {
            return e.toString();
        }
        return "Hotel " + hotel.getName() + " has been added to the database with the following details:"
                + "\nStars - " + hotel.getStars()
                + "\nRating - " + hotel.getRating();
    }

    //tot cu postman poate edita un hotel daca stie id-ul
    @RequestMapping(value = "/hotels/editHotel/put/{id}", method = RequestMethod.PUT)
    public String editHotel(@PathVariable(value = "id") Integer id,
                            @RequestParam("stars") Integer stars,
                            @RequestParam("rating") Float rating) {
        if (hotelDao.idExists(id)) {
            try {
                hotel = hotelDao.modifyHotelByID(id, stars, rating);
            } catch (Exception e) {
                e.toString();
            }
            return "Hotel with ID: " + id + ", " + hotel.getName() + " has been modified and now has the following details:"
                    + "\nStars - " + hotel.getStars()
                    + "\nRating - " + hotel.getRating();
        } else {
            return "Hotel with ID: " + id + " does not exist!";
        }
    }

    //daca stie id-ul si intra pe ramura cu /delete va sterge linia asociata id-ului
    @RequestMapping(value = "/hotels/deleteHotel/delete/{id}", method = RequestMethod.DELETE)
    public String deleteHotel(@PathVariable(value = "id") Integer id) {
        if (hotelDao.idExists(id)) {
            try {
                hotel = hotelDao.returnHotel(id);
            } catch (Exception e) {
                return e.toString();
            }
            try {
                hotelDao.removeHotel(id);
            } catch (Exception e) {
                return e.toString();
            }
            return "Hotel with ID: " + id + " and the following details:"
                    + "\nName - " + hotel.getName()
                    + "\nStars - " + hotel.getStars()
                    + "\nRating - " + hotel.getRating()
                    + "\nhas been successfully deleted from the database.";
        } else {return "Hotel with ID: " + id + " does not exist!";}
    }

}



